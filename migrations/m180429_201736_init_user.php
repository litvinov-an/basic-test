<?php

use yii\db\Migration;

class m180429_201736_init_user extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'id' => 1,
            'fio' => 'admin',
            'username' => 'admin',
            'password_hash' => '$2y$13$2mU1uc7UuhOQMJbPyZ9ynOL0KLiWLX06CJjo1ArGS78r/Ao6WdCwe',
            'auth_key' => 'Ijw2fHuPrgqh9gbTtmYMpuXUzmiH51Tn',
        ]);
    }

    public function safeDown()
    {
        echo "m180428_181322_init_user cannot be reverted.\n";

        return false;
    }
}
