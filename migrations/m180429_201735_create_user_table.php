<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180429_201735_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'fio' => $this->string(),
            'auth_key' => $this->string(),
            'password_hash' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
