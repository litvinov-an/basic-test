<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Проекты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">
    <div class="box">
        <div class="box-header">

            <p>
                <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалит'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>

        <div class="box-body" style="width: auto">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'label' => 'Связанный пользователь',
                        'value' => function ($id) {
                            return User::find()->where(['id' => $id])->one()->username;
                        }
                    ],
                    'title',
                    'cost',
                    'date_start',
                    'date_release',
                ],
            ]) ?>
        </div>
    </div>
</div>
