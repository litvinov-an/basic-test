<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div class="box">
        <div class="box-header">
            <p>
                <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        </div>

        <div class="box-body" style="width: auto">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',
                    'fio',
                ],
            ]) ?>
        </div>
    </div>
</div>
