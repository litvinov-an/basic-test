<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Пользователи', 'url' => ['/user'], 'visible' => !(Yii::$app->user->isGuest)],
                    ['label' => 'Проекты', 'url' => ['/project'], 'visible' => !(Yii::$app->user->isGuest)],
                    ['label' => 'Войти', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Зарегистрироваться', 'url' => ['/site/signup'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
